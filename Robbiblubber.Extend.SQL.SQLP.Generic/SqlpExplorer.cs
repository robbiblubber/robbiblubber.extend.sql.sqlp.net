﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.SQL.Interpreter;
using Robbiblubber.Util.Controls;
using Robbiblubber.Util.SQL.Exploration.MSSQL;
using Robbiblubber.Util.SQL.Exploration.Oracle;
using Robbiblubber.Util.SQL.Exploration.MySQL;
using Robbiblubber.Util.SQL.Exploration.SQLite;
using Robbiblubber.Util.SQL.Exploration.PostgreSQL;



namespace Robbiblubber.Extend.SQL.SQLP.Generic
{
    /// <summary>This class represents the SQLP database structure.</summary>
    public class SqlpExplorer: IExporer, IDisposable, IAutoCompletionSource
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Provider.</summary>
        protected ProviderItem _Provider = null;

        /// <summary>Default keywords.</summary>
        protected IExporer _Explorer = null;

        /// <summary>Default items.</summary>
        protected DbItem[] _Items = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider"></param>
        public SqlpExplorer(ProviderItem provider)
        {
            _Provider = provider;

            switch((string) provider.GetData("sqlpprovider"))
            {
                case "mssql":
                    _Explorer = new MsExplorer(provider); break;
                case "oracle":
                    _Explorer = new OraExplorer(provider); break;
                case "mysql":
                case "mariadb":
                    _Explorer = new MyExplorer(provider); break;
                case "pgsql":
                    _Explorer = new PgExplorer(provider); break;
                case "sqlite":
                    _Explorer = new LiteExplorer(provider); break;
                default:
                    _Items = new DbItem[] { new SqlpDatabase(provider) };
                    break;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExplorer                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the items root for this explorer.</summary>
        public DbItem[] Items
        {
            get
            {
                if(_Explorer == null) { return _Items; }
                return _Explorer.Items;
            }
        }


        /// <summary>Initializes auto completion.</summary>
        /// <param name="a">AutoCompletion instance.</param>
        public void InitAutoCompletion(AutoCompletion a)
        {
            if(_Explorer != null) { _Explorer.InitAutoCompletion(a); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAutoCompletionSource                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the words for a given preceding text.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Word list.</returns>
        public IEnumerable<IAutoCompletionKeyword> UpdateWords(string pretext, char key)
        {
            if(_Explorer != null) { return _Explorer.UpdateWords(pretext, key); }

            return new IAutoCompletionKeyword[0];
        }


        /// <summary>Returns if the words need to be updated.</summary>
        /// <param name="pretext">Preceding text.</param>
        /// <param name="key">Current key.</param>
        /// <returns>Returns TRUE if an update is required, otherwise returns FALSE.</returns>
        public bool UpdateRequired(string pretext, char key)
        {
            if(_Explorer != null) { return _Explorer.UpdateRequired(pretext, key); }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}
    }
}
