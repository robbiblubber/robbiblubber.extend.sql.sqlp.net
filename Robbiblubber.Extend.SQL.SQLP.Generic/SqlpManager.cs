﻿using System;
using System.Drawing;

using Robbiblubber.Util.SQL.Interpreter;



namespace Robbiblubber.Extend.SQL.SQLP.Generic
{
    /// <summary>This class implements the SQLP database manager.</summary>
    public sealed class SqlpManager: SQLProviderManager, IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProviderManager                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get { return "Robbiblubber.Extend.SQL.SQLP.Data.SqlpProvider"; }
        }


        /// <summary>Gets the manager display name.</summary>
        public override string DisplayName
        {
            get { return "SQLP (Generic Database)"; }
        }


        /// <summary>Gets the provider control.</summary>

        public override IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new SqlpProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public override Image ProviderIcon
        {
            get { return Resources.sqlp; }
        }


        /// <summary>Gets the connction icon.</summary>
        public override Image ConnectionIcon
        {
            get { return Resources.sqlpdb; }
        }
    }
}