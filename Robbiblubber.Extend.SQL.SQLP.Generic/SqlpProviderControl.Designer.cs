﻿namespace Robbiblubber.Extend.SQL.SQLP.Generic
{
    partial class SqlpProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._TextString = new System.Windows.Forms.TextBox();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelString = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _TextString
            // 
            this._TextString.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextString.Location = new System.Drawing.Point(30, 85);
            this._TextString.Name = "_TextString";
            this._TextString.Size = new System.Drawing.Size(403, 25);
            this._TextString.TabIndex = 1;
            this._TextString.TextChanged += new System.EventHandler(this._TextString_TextChanged);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 5;
            this._ButtonTest.Tag = "sqlpsi::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelString
            // 
            this._LabelString.AutoSize = true;
            this._LabelString.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelString.Location = new System.Drawing.Point(27, 69);
            this._LabelString.Name = "_LabelString";
            this._LabelString.Size = new System.Drawing.Size(104, 13);
            this._LabelString.TabIndex = 1;
            this._LabelString.Tag = "sqlpsi::udiag.connect.str";
            this._LabelString.Text = "&Connection String:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlpsi::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // SqlpProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelString);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextString);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SqlpProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextString;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelString;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.ToolTip _ToolTip;
    }
}
