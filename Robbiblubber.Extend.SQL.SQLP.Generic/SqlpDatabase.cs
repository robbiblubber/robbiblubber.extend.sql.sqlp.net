﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Robbiblubber.Util.Library.Configuration;
using Robbiblubber.Util.SQL.Interpreter;



namespace Robbiblubber.Extend.SQL.SQLP.Generic
{
    /// <summary>This class represents a SQLP database.</summary>
    public class SqlpDatabase: DbItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database name.</summary>
        private string _Name;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="provider">Provider.</param>
        internal SqlpDatabase(ProviderItem provider)
        {
            Provider = provider;
            _Name = provider.Data.GetDdpValue("enhanced").GetDdpValue("url");

            _Children = new List<DbItem>();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider.</summary>
        public ProviderItem Provider { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item name.</summary>
        public override string Name
        {
            get { return _Name; }
        }


        /// <summary>Gets the item image.</summary>
        public override Image Icon
        {
            get { return Resources.sqlpdb; }
        }


        /// <summary>Gets the icon key.</summary>
        public override string IconKey
        {
            get { return "sqlp::database"; }
        }


        /// <summary>Gets the object display text.</summary>
        public override string DisplayText
        {
            get { return _Name; }
        }
    }
}
