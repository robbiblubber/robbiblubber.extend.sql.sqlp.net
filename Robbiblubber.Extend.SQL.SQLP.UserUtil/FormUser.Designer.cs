﻿namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    partial class FormUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUser));
            this._TextUserName = new System.Windows.Forms.TextBox();
            this._LabelUserName = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LabelPassword = new System.Windows.Forms.Label();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _TextUserName
            // 
            this._TextUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUserName.Location = new System.Drawing.Point(33, 40);
            this._TextUserName.Name = "_TextUserName";
            this._TextUserName.Size = new System.Drawing.Size(356, 25);
            this._TextUserName.TabIndex = 0;
            this._TextUserName.TextChanged += new System.EventHandler(this._TextCustom_TextChanged);
            // 
            // _LabelUserName
            // 
            this._LabelUserName.AutoSize = true;
            this._LabelUserName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUserName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelUserName.Location = new System.Drawing.Point(30, 23);
            this._LabelUserName.Name = "_LabelUserName";
            this._LabelUserName.Size = new System.Drawing.Size(65, 13);
            this._LabelUserName.TabIndex = 0;
            this._LabelUserName.Tag = "sqlpuu::udiag.user.name";
            this._LabelUserName.Text = "User &Name:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(33, 91);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(356, 25);
            this._TextPassword.TabIndex = 1;
            this._TextPassword.UseSystemPasswordChar = true;
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelPassword.Location = new System.Drawing.Point(30, 74);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 1;
            this._LabelPassword.Tag = "sqlpuu::udiag.user.pwd";
            this._LabelPassword.Text = "&Password:";
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(264, 148);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 3;
            this._ButtonCancel.Tag = "sqlpuu::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(133, 148);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 2;
            this._ButtonOK.Tag = "sqlpuu::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // FormUser
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(437, 197);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._LabelPassword);
            this.Controls.Add(this._TextUserName);
            this.Controls.Add(this._LabelUserName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUser";
            this.Tag = "sqlpuu::udiag.user.caption";
            this.Text = "New User";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextUserName;
        private System.Windows.Forms.Label _LabelUserName;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
    }
}