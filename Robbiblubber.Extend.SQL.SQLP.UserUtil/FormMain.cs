﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Configuration;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class implements the main window.</summary>
    public partial class FormMain: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Users.</summary>
        internal TUserCollection __Users = null;

        /// <summary>Current file name.</summary>
        private string _FileName = null;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;

        /// <summary>Show debug flag.</summary>
        private bool _ShowDebug = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="file">File name.</param>
        public FormMain(string file = null)
        {
            InitializeComponent();

            this.RestoreLayout();

            Locale.UsingPrefixes("debugx", "sqlpuu");
            Locale.LoadSelection("Robbiblubber.Util.NET");
            this.Localize();

            _LoadSettings();

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            _UpdateRecentFiles();

            _SetTitle();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the debug menu is shown.</summary>
        public bool ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Checks for unsaved changes.</summary>
        /// <returns>Returns TRUE if action can procees, FALSE if the action needs to be cancelled for unsaved changes.</returns>
        private bool _CheckCancelUnsaved()
        {
            if(__Users == null) { return true; }
            if(__Users.Virgin)  { return true; }

            DialogResult d = MessageBox.Show("sqlpuu::udiag.unsaved.text".Localize("The current file contains unsaved changes. Save these chages before proceeding?"), "sqluu::udiag.unsaved.caption".Localize("Save Changes"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if(d == DialogResult.Cancel) { return false; }

            if(d == DialogResult.Yes)
            {
                return _Save();
            }

            return true;
        }


        /// <summary>Saves the current file.</summary>
        /// <returns>Returns TRUE if file has been saved, returns FALSE if action has been cancelled.</returns>
        private bool _Save()
        {
            if(__Users == null) return true;

            if(string.IsNullOrEmpty(_FileName)) { return _SaveAs(); }

            try
            {
                __Users.Save(_FileName);

                _RecentFiles.Add(_FileName);
                _UpdateRecentFiles();

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlpuu::udiag.save.fail".Localize("The file could not be saved. $(1)").Replace("$(1)", ex.Message), "sqlpuu::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        /// <summary>Saves the current file with a new name.</summary>
        /// <returns>Returns TRUE if file has been saved, returns FALSE if action has been cancelled.</returns>
        private bool _SaveAs()
        {
            if(__Users == null) return true;

            try
            {
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "sqlpuu::udiag.filter.aclf".Localize("Access Control Files") + "(*.aclf)|*.aclf|" + "sqlpuu::udiag.filter.all".Localize("All files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    _FileName = d.FileName;
                    _Save();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00204", ex); }

            return false;
        }


        /// <summary>Loads a file.</summary>
        /// <param name="fileName">File name.</param>
        private void _Load(string fileName)
        {
            try
            {
                __Users = TUserCollection.Load(fileName);
                _FileName = fileName;

                _RecentFiles.Add(fileName);
                _UpdateRecentFiles();
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlpuu::udiag.open.failed".Localize("Failed to open file. $(1)").Replace("$(1)", ex.Message), "sqlpuu.udiag.open.caption".Localize("Open"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _Show();
        }


        /// <summary>Shows the current file.</summary>
        private void _Show()
        {
            _ListUsers.BeginUpdate();

            _ListUsers.Items.Clear();

            if(__Users != null)
            {
                foreach(TUser i in __Users.OrderBy(m => m.Name))
                {
                    ListViewItem l = new ListViewItem(i.Name, "ico:user");
                    l.Tag = i;
                    _ListUsers.Items.Add(l);
                }
            }

            _ListUsers.EndUpdate();
            _SetTitle();
        }


        /// <summary>Sets the window title.</summary>
        private void _SetTitle()
        {
            if(__Users == null)
            {
                Text = "sqlpuu::udiag.window.caption".Localize("SQLP User Utility");
                _ListUsers.Visible = false;
            }
            else
            {
                _ListUsers.Visible = true;

                if(string.IsNullOrEmpty(_FileName))
                {
                    Text = "sqlpuu::common.unnamed".Localize("Unnamed") + "* - " + "sqlpuu::udiag.window.caption".Localize("SQLP User Utility");
                }
                else
                {
                    Text = Path.GetFileName(_FileName) + (__Users.Virgin ? " - " : "* - ") + "sqlpuu::udiag.window.caption".Localize("SQLP User Utility");
                }
            }
        }


        /// <summary>Saves settings.</summary>
        private void _SaveSettings()
        {
            try
            {
                ConfigFile cfg = ConfigFile.Load(PathOp.UserSettingsFile);
                cfg.SetBoolean("/settings/debug", ShowDebug);
                cfg.Save();
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00110", ex); }
        }


        /// <summary>Loads settings.</summary>
        private void _LoadSettings()
        {
            try
            {
                ConfigFile cfg = ConfigFile.Load(PathOp.UserSettingsFile);
                ShowDebug = cfg.GetBoolean("/settings/debug");
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00111", ex); }
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), (i.ToLower().EndsWith(".aclf") ? Properties.Resources.aclf : Properties.Resources.document), new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if(_FileName != null) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(_FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00109", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            new FormAbout().ShowDialog();
        }


        /// <summary>Form closing.</summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!_CheckCancelUnsaved())
            {
                e.Cancel = true;
                return;
            }

            this.SaveLayout();
        }


        /// <summary>Menu "New" click.</summary>
        private void _MenuNew_Click(object sender, EventArgs e)
        {
            if(!_CheckCancelUnsaved()) { return; }

            __Users = new TUserCollection();
            _FileName = null;

            _UpdateRecentFiles();
            _Show();
        }


        /// <summary>List resize.</summary>
        private void _ListUsers_Resize(object sender, EventArgs e)
        {
            _ChName.Width = (_ListUsers.Width - 32);
        }


        /// <summary>Menu "New User" click.</summary>
        private void _MenuNewUser_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;

            FormUser f = new FormUser();

            if(f.ShowDialog() == DialogResult.OK)
            {
                _Show();
            }
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _MenuDeleteUser_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;
            if(_ListUsers.SelectedItems.Count == 0) return;

            DialogResult d;
            if(_ListUsers.SelectedItems.Count == 1)
            {
                d = MessageBox.Show("sqlpuu::udiag.delete.single".Localize("Do you really want to delete user \"$(1)\"?").Replace("$(1)", ((TUser) _ListUsers.SelectedItems[0].Tag).Name), "sqlpuu::udiag.delete.caption".Localize("Delete"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            else
            {
                d = MessageBox.Show("sqlpuu::udiag.delete.multiple".Localize("Do you really want to delete these $(1) users?").Replace("$(1)", _ListUsers.SelectedItems.Count.ToString()), "sqlpuu::udiag.delete.caption".Localize("Delete"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }

            if(d == DialogResult.Yes)
            {
                foreach(ListViewItem i in _ListUsers.SelectedItems)
                {
                    ((TUser) i.Tag).Delete();
                }

                _Show();
            }
        }


        /// <summary>Menu "Rename User" click.</summary>
        private void _MenuRenameUser_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;
            if(_ListUsers.SelectedItems.Count != 1) return;

            FormRename f = new FormRename((TUser) _ListUsers.SelectedItems[0].Tag);
            if(f.ShowDialog() == DialogResult.OK)
            {
                _Show();
            }
        }


        /// <summary>Menu "Set Password" click.</summary>
        private void _MenuSetPassword_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;
            if(_ListUsers.SelectedItems.Count != 1) return;

            FormPassword f = new FormPassword((TUser) _ListUsers.SelectedItems[0].Tag);
            if(f.ShowDialog() == DialogResult.OK)
            {
                _SetTitle();
            }
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            if(__Users == null)
            {
                _MenuClose.Enabled = _MenuSave.Enabled = _MenuSaveAs.Enabled = false;
            }
            else
            {
                _MenuSave.Enabled = (!(__Users.Virgin || string.IsNullOrEmpty(_FileName)));
            }
        }


        /// <summary>Menu "File" closing.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            _MenuClose.Enabled = _MenuSave.Enabled = _MenuSaveAs.Enabled = true;
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSave_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;

            _Save();
            _SetTitle();
        }


        /// <summary>Menu "Save as" click.</summary>
        private void _MenuSaveAs_Click(object sender, EventArgs e)
        {
            if(__Users == null) return;

            _SaveAs();
            _SetTitle();
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void _MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            _MenuNewUser.Enabled = (__Users != null);
            _MenuDeleteUser.Enabled = (_ListUsers.SelectedItems.Count > 0);
            _MenuRenameUser.Enabled = _MenuSetPassword.Enabled = (_ListUsers.SelectedItems.Count == 1);
        }


        /// <summary>Menu "Edit" closed.</summary>
        private void _MenuEdit_DropDownClosed(object sender, EventArgs e)
        {
            _MenuNewUser.Enabled = _MenuDeleteUser.Enabled = _MenuRenameUser.Enabled = _MenuSetPassword.Enabled = true;
        }


        /// <summary>Menu "Show Help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-SQLPUU");
        }


        /// <summary>Recent file click.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            if(_CheckCancelUnsaved())
            {
                _Load((string) ((ToolStripMenuItem) sender).Tag);
            }
        }


        /// <summary>Menu "Open" click.</summary>
        private void _MenuOpen_Click(object sender, EventArgs e)
        {
            if(_CheckCancelUnsaved())
            {
                try
                {
                    OpenFileDialog d = new OpenFileDialog();
                    d.Filter = "sqlpuu::udiag.filter.aclf".Localize("Access Control Files") + "(*.aclf)|*.aclf|" + "sqlpuu::udiag.filter.all".Localize("All files") + "|*.*";

                    if(d.ShowDialog() == DialogResult.OK)
                    {
                        _Load(d.FileName);
                    }
                }
                catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00207", ex); }
            }
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            try
            {
                FormSettings f = new FormSettings();

                if(f.ShowDialog() == DialogResult.OK)
                {
                    ShowDebug = f.ShowDebug;
                    _SaveSettings();

                    if(f.SelectedLocale != Locale.Selected)
                    {
                        Locale.Selected = f.SelectedLocale;
                        Locale.SaveSelection("Robbiblubber.Util.NET");
                        this.Localize();
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLUU00149", ex); }
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00150", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00151", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/robbiblubber.util/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("SQLPUU00152", ex); }
        }


        /// <summary>Context menu opening.</summary>
        private void _CmenuUsers_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _CmenuDeleteUser.Enabled = (_ListUsers.SelectedItems.Count > 0);
            _CmenuRenameUser.Enabled = _CmenuSetPassword.Enabled = (_ListUsers.SelectedItems.Count == 1);
        }


        /// <summary>Menu "Close" click.</summary>
        private void _MenuClose_Click(object sender, EventArgs e)
        {
            if(_CheckCancelUnsaved())
            {
                __Users = null;
                _FileName = null;

                _UpdateRecentFiles();
                _Show();
            }
        }
    }
}
