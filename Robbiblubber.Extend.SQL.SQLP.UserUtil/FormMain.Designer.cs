﻿namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNewUser = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDeleteUser = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRenameUser = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSetPassword = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._ListUsers = new System.Windows.Forms.ListView();
            this._ChName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._IlistUsers = new System.Windows.Forms.ImageList(this.components);
            this._CmenuUsers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuNewUser = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDeleteUser = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuRenameUser = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuSetPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this._CmenuUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuTools,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(645, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNew,
            this._MenuOpen,
            this._MenuSave,
            this._MenuSaveAs,
            this._MenuFileBlank0,
            this._MenuClose,
            this._MenuFileBlank1,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 19);
            this._MenuFile.Tag = "sqlpuu::menu.file";
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuNew
            // 
            this._MenuNew.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNew.Image")));
            this._MenuNew.Name = "_MenuNew";
            this._MenuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuNew.Size = new System.Drawing.Size(195, 22);
            this._MenuNew.Tag = "sqlpuu::menu.new";
            this._MenuNew.Text = "&New";
            this._MenuNew.Click += new System.EventHandler(this._MenuNew_Click);
            // 
            // _MenuOpen
            // 
            this._MenuOpen.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpen.Image")));
            this._MenuOpen.Name = "_MenuOpen";
            this._MenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpen.Size = new System.Drawing.Size(195, 22);
            this._MenuOpen.Tag = "sqlpuu::menu.open";
            this._MenuOpen.Text = "&Open...";
            this._MenuOpen.Click += new System.EventHandler(this._MenuOpen_Click);
            // 
            // _MenuSave
            // 
            this._MenuSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSave.Image")));
            this._MenuSave.Name = "_MenuSave";
            this._MenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSave.Size = new System.Drawing.Size(195, 22);
            this._MenuSave.Tag = "sqlpuu::menu.save";
            this._MenuSave.Text = "&Save";
            this._MenuSave.Click += new System.EventHandler(this._MenuSave_Click);
            // 
            // _MenuSaveAs
            // 
            this._MenuSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSaveAs.Image")));
            this._MenuSaveAs.Name = "_MenuSaveAs";
            this._MenuSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this._MenuSaveAs.Size = new System.Drawing.Size(195, 22);
            this._MenuSaveAs.Tag = "sqlpuu::menu.as";
            this._MenuSaveAs.Text = "Save &As...";
            this._MenuSaveAs.Click += new System.EventHandler(this._MenuSaveAs_Click);
            // 
            // _MenuFileBlank0
            // 
            this._MenuFileBlank0.Name = "_MenuFileBlank0";
            this._MenuFileBlank0.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuClose
            // 
            this._MenuClose.Image = ((System.Drawing.Image)(resources.GetObject("_MenuClose.Image")));
            this._MenuClose.Name = "_MenuClose";
            this._MenuClose.Size = new System.Drawing.Size(195, 22);
            this._MenuClose.Tag = "sqlpuu::menu.close";
            this._MenuClose.Text = "&Close";
            this._MenuClose.Click += new System.EventHandler(this._MenuClose_Click);
            // 
            // _MenuFileBlank1
            // 
            this._MenuFileBlank1.Name = "_MenuFileBlank1";
            this._MenuFileBlank1.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(195, 22);
            this._MenuRecent.Tag = "sqlpuu::menu.recent";
            this._MenuRecent.Text = "&Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._MenuExit.Size = new System.Drawing.Size(195, 22);
            this._MenuExit.Tag = "sqlpuu::menu.exit";
            this._MenuExit.Text = "&Exit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNewUser,
            this._MenuDeleteUser,
            this._MenuRenameUser,
            this._MenuSetPassword});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 19);
            this._MenuEdit.Tag = "sqlpuu::menu.edit";
            this._MenuEdit.Text = "&Edit";
            this._MenuEdit.DropDownClosed += new System.EventHandler(this._MenuEdit_DropDownClosed);
            this._MenuEdit.DropDownOpening += new System.EventHandler(this._MenuEdit_DropDownOpening);
            // 
            // _MenuNewUser
            // 
            this._MenuNewUser.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNewUser.Image")));
            this._MenuNewUser.Name = "_MenuNewUser";
            this._MenuNewUser.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this._MenuNewUser.Size = new System.Drawing.Size(197, 22);
            this._MenuNewUser.Tag = "sqlpuu::menu.newuser";
            this._MenuNewUser.Text = "&New User...";
            this._MenuNewUser.Click += new System.EventHandler(this._MenuNewUser_Click);
            // 
            // _MenuDeleteUser
            // 
            this._MenuDeleteUser.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDeleteUser.Image")));
            this._MenuDeleteUser.Name = "_MenuDeleteUser";
            this._MenuDeleteUser.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this._MenuDeleteUser.Size = new System.Drawing.Size(197, 22);
            this._MenuDeleteUser.Tag = "sqlpuu::menu.deluser";
            this._MenuDeleteUser.Text = "&Delete";
            this._MenuDeleteUser.Click += new System.EventHandler(this._MenuDeleteUser_Click);
            // 
            // _MenuRenameUser
            // 
            this._MenuRenameUser.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRenameUser.Image")));
            this._MenuRenameUser.Name = "_MenuRenameUser";
            this._MenuRenameUser.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this._MenuRenameUser.Size = new System.Drawing.Size(197, 22);
            this._MenuRenameUser.Tag = "sqlpuu::menu.renuser";
            this._MenuRenameUser.Text = "&Rename...";
            this._MenuRenameUser.Click += new System.EventHandler(this._MenuRenameUser_Click);
            // 
            // _MenuSetPassword
            // 
            this._MenuSetPassword.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSetPassword.Image")));
            this._MenuSetPassword.Name = "_MenuSetPassword";
            this._MenuSetPassword.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this._MenuSetPassword.Size = new System.Drawing.Size(197, 22);
            this._MenuSetPassword.Tag = "sqlpuu::menu.pwd";
            this._MenuSetPassword.Text = "Set &Password...";
            this._MenuSetPassword.Click += new System.EventHandler(this._MenuSetPassword_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(47, 19);
            this._MenuTools.Text = "&Tools";
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(125, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "&Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "&Save Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(125, 22);
            this._MenuSettings.Tag = "sqlpuu::menu.settings";
            this._MenuSettings.Text = "&Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 19);
            this.helpToolStripMenuItem.Tag = "sqlpuu::menu.help";
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(159, 22);
            this._MenuShowHelp.Tag = "sqlpuu::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help...";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(159, 22);
            this._MenuAbout.Tag = "sqlpuu::menu.about";
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _ListUsers
            // 
            this._ListUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ListUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._ChName});
            this._ListUsers.ContextMenuStrip = this._CmenuUsers;
            this._ListUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this._ListUsers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._ListUsers.Location = new System.Drawing.Point(0, 25);
            this._ListUsers.Name = "_ListUsers";
            this._ListUsers.Size = new System.Drawing.Size(645, 383);
            this._ListUsers.SmallImageList = this._IlistUsers;
            this._ListUsers.TabIndex = 1;
            this._ListUsers.UseCompatibleStateImageBehavior = false;
            this._ListUsers.View = System.Windows.Forms.View.Details;
            this._ListUsers.Resize += new System.EventHandler(this._ListUsers_Resize);
            // 
            // _ChName
            // 
            this._ChName.Tag = "sqlpuu::udiag.username";
            this._ChName.Text = "User Name";
            this._ChName.Width = 614;
            // 
            // _IlistUsers
            // 
            this._IlistUsers.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistUsers.ImageStream")));
            this._IlistUsers.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistUsers.Images.SetKeyName(0, "ico:user");
            // 
            // _CmenuUsers
            // 
            this._CmenuUsers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuNewUser,
            this._CmenuDeleteUser,
            this._CmenuRenameUser,
            this._CmenuSetPassword});
            this._CmenuUsers.Name = "_CmenuUsers";
            this._CmenuUsers.Size = new System.Drawing.Size(153, 92);
            this._CmenuUsers.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuUsers_Opening);
            // 
            // _CmenuNewUser
            // 
            this._CmenuNewUser.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuNewUser.Image")));
            this._CmenuNewUser.Name = "_CmenuNewUser";
            this._CmenuNewUser.Size = new System.Drawing.Size(152, 22);
            this._CmenuNewUser.Tag = "sqlpuu::menu.newuser";
            this._CmenuNewUser.Text = "New User...";
            this._CmenuNewUser.Click += new System.EventHandler(this._MenuNewUser_Click);
            // 
            // _CmenuDeleteUser
            // 
            this._CmenuDeleteUser.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDeleteUser.Image")));
            this._CmenuDeleteUser.Name = "_CmenuDeleteUser";
            this._CmenuDeleteUser.Size = new System.Drawing.Size(152, 22);
            this._CmenuDeleteUser.Tag = "sqlpuu::menu.deluser";
            this._CmenuDeleteUser.Text = "Delete";
            this._CmenuDeleteUser.Click += new System.EventHandler(this._MenuDeleteUser_Click);
            // 
            // _CmenuRenameUser
            // 
            this._CmenuRenameUser.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuRenameUser.Image")));
            this._CmenuRenameUser.Name = "_CmenuRenameUser";
            this._CmenuRenameUser.Size = new System.Drawing.Size(152, 22);
            this._CmenuRenameUser.Tag = "sqlpuu::menu.renuser";
            this._CmenuRenameUser.Text = "Rename...";
            this._CmenuRenameUser.Click += new System.EventHandler(this._MenuRenameUser_Click);
            // 
            // _CmenuSetPassword
            // 
            this._CmenuSetPassword.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuSetPassword.Image")));
            this._CmenuSetPassword.Name = "_CmenuSetPassword";
            this._CmenuSetPassword.Size = new System.Drawing.Size(152, 22);
            this._CmenuSetPassword.Tag = "sqlpuu::menu.pwd";
            this._CmenuSetPassword.Text = "Set Password...";
            this._CmenuSetPassword.Click += new System.EventHandler(this._MenuSetPassword_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 408);
            this.Controls.Add(this._ListUsers);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Tag = "sqlpuu::udiag.window.caption";
            this.Text = "SQLP User Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this._CmenuUsers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuNew;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpen;
        private System.Windows.Forms.ToolStripMenuItem _MenuSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuNewUser;
        private System.Windows.Forms.ToolStripMenuItem _MenuDeleteUser;
        private System.Windows.Forms.ToolStripMenuItem _MenuRenameUser;
        private System.Windows.Forms.ToolStripMenuItem _MenuSetPassword;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.ListView _ListUsers;
        private System.Windows.Forms.ColumnHeader _ChName;
        private System.Windows.Forms.ImageList _IlistUsers;
        private System.Windows.Forms.ToolStripMenuItem _MenuClose;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank1;
        private System.Windows.Forms.ContextMenuStrip _CmenuUsers;
        private System.Windows.Forms.ToolStripMenuItem _CmenuNewUser;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDeleteUser;
        private System.Windows.Forms.ToolStripMenuItem _CmenuRenameUser;
        private System.Windows.Forms.ToolStripMenuItem _CmenuSetPassword;
    }
}

