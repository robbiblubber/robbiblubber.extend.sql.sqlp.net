﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class implements the rename user window.</summary>
    public partial class FormRename: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>User.</summary>
        private TUser _User;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cretaes a new instance of this class.</summary>
        /// <param name="user">User.</param>
        public FormRename(TUser user)
        {
            _User = user;
            InitializeComponent();

            _TextUserName.Text = _User.Name;
            _TextUserName_TextChanged(null, null);

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>User name changed.</summary>
        private void _TextUserName_TextChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(_TextUserName.Text))
            {
                _ButtonOK.Enabled = false;
            }
            else
            {
                _ButtonOK.Enabled = (!Program.__Main.__Users.Contains(_TextUserName.Text));
            }
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            _User.Name = _TextUserName.Text;
            Close();
        }
    }
}
