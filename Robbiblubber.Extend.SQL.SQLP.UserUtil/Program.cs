﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Splash window.</summary>
        internal static FormSplash __Splash;

        /// <summary>Main window.</summary>
        internal static FormMain __Main;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Main entry point                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="argv">Arguments.</param>
        [STAThread]
        static void Main(string[] argv)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            __Splash = new FormSplash();
            __Splash.Show();
            Application.DoEvents();

            PathOp.ApplicationPart = "robbiblubber.org/UserUtil";

            string file = null;

            if(argv.Length > 0) { file = argv[0]; }

            __Splash.DelayClose();

            Application.Run(__Main = new FormMain(file));
        }
    }
}
