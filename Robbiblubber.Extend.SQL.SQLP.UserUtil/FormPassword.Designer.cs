﻿namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    partial class FormPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPassword));
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LabelPassword = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(264, 93);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 11;
            this._ButtonCancel.Tag = "sqlpuu::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(133, 93);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 10;
            this._ButtonOK.Tag = "sqlpuu::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(33, 40);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(356, 25);
            this._TextPassword.TabIndex = 8;
            this._TextPassword.UseSystemPasswordChar = true;
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelPassword.Location = new System.Drawing.Point(30, 23);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 9;
            this._LabelPassword.Tag = "sqlpuu::udiag.user.pwd";
            this._LabelPassword.Text = "&Password:";
            // 
            // FormPassword
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(437, 149);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._LabelPassword);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPassword";
            this.Tag = "sqlpuu:udaig.pwd.caption";
            this.Text = "Set Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.Label _LabelPassword;
    }
}