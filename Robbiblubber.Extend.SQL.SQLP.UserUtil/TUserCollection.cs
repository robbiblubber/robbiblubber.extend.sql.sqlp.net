﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Util.Library;

using Robbiblubber.Util.Library.Configuration;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class represents a user collection.</summary>
    public sealed class TUserCollection: IEnumerable<TUser>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saved string.</summary>
        private string _SavedString = "*";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        internal Dictionary<string, TUser> __Items = new Dictionary<string, TUser>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TUserCollection()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        public TUserCollection(string fileName)
        {
            DdpString ddp = DdpString.FromFile(fileName);

            foreach(string i in ddp.Keys)
            {
                TUser.__Feed(this, i, ddp.GetString(i));
            }

            _SavedString = ddp.ToString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a user collection.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>User collection.</returns>
        public static TUserCollection Load(string fileName)
        {
            return new TUserCollection(fileName);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a user object by its name.</summary>
        /// <param name="name">User name.</param>
        /// <returns>User</returns>
        public TUser this[string name]
        {
            get { return __Items[name]; }
        }


        /// <summary>Gets if the collection is unchanged.</summary>
        public bool Virgin
        {
            get { return (_SavedString == ToString()); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the ddp for this instance.</summary>
        private DdpString _Ddp
        {
            get
            {
                DdpString rval = new DdpString();

                foreach(TUser i in __Items.Values)
                {
                    i.__Save(rval);
                }

                return rval;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the collection contains a user with this name.</summary>
        /// <param name="userName">User name.</param>
        /// <returns>Returns TRUE if the collection contains the user name, otherwise returns FALSE.</returns>
        public bool Contains(string userName)
        {
            return __Items.ContainsKey(userName);
        }
        

        /// <summary>Returns if the collection contains a user.</summary>
        /// <param name="user">User.</param>
        /// <returns>Returns TRUE if the collection contains the user, otherwise returns FALSE.</returns>
        public bool Contains(TUser user)
        {
            return __Items.ContainsKey(user.Name);
        }


        /// <summary>Adds a user to the collection.</summary>
        /// <param name="name">User name.</param>
        /// <param name="password">Password.</param>
        public void Add(string name, string password = null)
        {
            new TUser(this, name, password);
        }


        /// <summary>Removes a user from the collection.</summary>
        /// <param name="name">User name.</param>
        public void Remove(string name)
        {
            Remove(__Items[name]);
        }


        /// <summary>Removes a user from the collection.</summary>
        /// <param name="user">User.</param>
        public void Remove(TUser user)
        {
            user.Delete();
        }


        /// <summary>Saves the collection.</summary>
        /// <param name="fileName">File name.</param>
        public void Save(string fileName)
        {
            _Ddp.Save(fileName);
            _SavedString = _Ddp.ToString();
        }
        



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return _Ddp.ToString();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<TUser>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<TUser> IEnumerable<TUser>.GetEnumerator()
        {
            return __Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return __Items.Values.GetEnumerator();
        }
    }
}
