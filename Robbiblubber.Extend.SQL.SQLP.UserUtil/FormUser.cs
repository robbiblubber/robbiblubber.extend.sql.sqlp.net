﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class implements the create user window.</summary>
    public partial class FormUser: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cretaes a new instance of this class.</summary>
        public FormUser()
        {
            InitializeComponent();

            _TextCustom_TextChanged(null, null);
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>User name changed.</summary>
        private void _TextCustom_TextChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(_TextUserName.Text))
            {
                _ButtonOK.Enabled = false;
            }
            else
            {
                _ButtonOK.Enabled = (!Program.__Main.__Users.Contains(_TextUserName.Text));
            }
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            Program.__Main.__Users.Add(_TextUserName.Text, _TextPassword.Text);
            Close();
        }
    }
}
