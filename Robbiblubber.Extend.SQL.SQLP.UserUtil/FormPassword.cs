﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class implements the change password window.</summary>
    public partial class FormPassword: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>User.</summary>
        private TUser _User;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cretaes a new instance of this class.</summary>
        /// <param name="user">User.</param>
        public FormPassword(TUser user)
        {
            _User = user;

            InitializeComponent();

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            _User.SetPassword(_TextPassword.Text);
            Close();
        }
    }
}
