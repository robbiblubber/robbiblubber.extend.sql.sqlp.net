﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.Library.Configuration;

namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class represents a user.</summary>
    public sealed class TUser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent collection.</summary>
        private TUserCollection _Parent;

        /// <summary>User name.</summary>
        private string _Name;

        /// <summary>Password.</summary>
        private string _Password;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent collection.</param>
        /// <param name="name">User name.</param>
        /// <param name="password">User password.</param>
        public TUser(TUserCollection parent, string name, string password = null)
        {
            if(password == null) { password = StringOp.Random(16); }

            _Parent = parent;
            _Name = name;
            SetPassword(password);

            _Parent.__Items.Add(name, this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        private TUser()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates an instance of this class for the given parameters.</summary>
        /// <param name="parent">Parent collection.</param>
        /// <param name="name">User name.</param>
        /// <param name="passwordHash">User password hash.</param>
        internal static void __Feed(TUserCollection parent, string name, string passwordHash)
        {
            TUser rval = new TUser();

            rval._Parent = parent;
            rval._Name = name;
            rval._Password = passwordHash;

            parent.__Items.Add(name, rval);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the user.</summary>
        /// <param name="ddp">ddp string.</param>
        internal void __Save(DdpString ddp)
        {
            ddp.SetValue(_Name, _Password);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the user name.</summary>
        public string Name
        {
            get { return _Name; }
            set
            {
                if(_Parent.Contains(value)) { _Parent.__Items.Add(value, null); }
                
                _Parent.__Items.Remove(_Name);
                _Name = value;
                _Parent.__Items.Add(_Name, this);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the password.</summary>
        /// <param name="password">Password.</param>
        public void SetPassword(string password)
        {
            _Password = SHA256.GetHash(password);
        }


        /// <summary>Deletes this object.</summary>
        public void Delete()
        {
            _Parent.__Items.Remove(_Name);
            _Name = _Password = null;
            _Parent = null;
        }
    }
}
