﻿using System.Windows.Forms;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Extend.SQL.SQLP.UserUtil
{
    /// <summary>This class implements the about window.</summary>
    internal partial class FormAbout: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormAbout()
        {
            InitializeComponent();

            this.Localize();
            _LabelVersion.Text = "dqöpuu::udiag.about.version".Localize("Version") + ' ' + VersionOp.ApplicationVersion.ToVersionString();
        }
    }
}
