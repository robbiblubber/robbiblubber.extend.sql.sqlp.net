﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.Library.Configuration;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements a data reader.</summary>
    public sealed class SqlpReader: IDataReader
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Column names.</summary>
        private List<string> _ColumnNames;

        /// <summary>Data.</summary>
        private List<List<string>> _Data;

        /// <summary>Current position.</summary>
        private int _Pos = -1;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="meta">Metadata.</param>
        /// <param name="data">Data.</param>
        internal SqlpReader(string meta, string data)
        {
            _ColumnNames = new List<string>();

            foreach(string i in meta.Split(':'))
            {
                if(string.IsNullOrEmpty(i)) continue;
                _ColumnNames.Add(i.FromBase64());
            }

            
            _Data = new List<List<string>>();

            if(!string.IsNullOrEmpty(data))
            {
                foreach(string i in data.Split('~'))
                {
                    List<string> v = new List<string>();

                    foreach(string j in i.Split(':'))
                    {
                        v.Add(j.FromBase64());
                    }
                    _Data.Add(v);
                }
            }

            IsClosed = false;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataReader                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the value of the specified column in its native format given the column name.</summary>
        /// <param name="name">Column name.</param>
        /// <returns>Value.</returns>
        public object this[string name]
        {
            get { return this[GetOrdinal(name)]; }
        }


        /// <summary>Gets the value of the specified column in its native format given the column ordinal.</summary>
        /// <param name="i">Column ordinal.</param>
        /// <returns>Value.</returns>
        public object this[int i]
        {
            get { return _Data[_Pos][i]; }
        }


        /// <summary>Gets the depth of nesting of the current row.</summary>
        /// <remarks>This is not supported and will always be zero.</remarks>
        public int Depth
        {
            get { return 0; }
        }


        /// <summary>Gets the number of fields in the current instance.</summary>
        public int FieldCount
        {
            get
            {
                if(_Data.Count == 0) { return 0; }

                return _Data[0].Count;
            }
        }


        /// <summary>Gets if the object is closed.</summary>
        public bool IsClosed
        {
            get; private set;
        }


        /// <summary>Gets the number of records affected.</summary>
        public int RecordsAffected
        {
            get { return 0; }
        }


        /// <summary>Closes the reader.</summary>
        public void Close()
        {
            IsClosed = true;
            _Data = null;
            _ColumnNames = null;
        }


        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {
            Close();
        }


        /// <summary>Gets the boolean value for a field.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public bool GetBoolean(int i)
        {
            string v = _Data[_Pos][i];

            int n;
            if(int.TryParse(v, out n))
            {
                return (n != 0);
            }

            return (v.ToLower().StartsWith("t") || v.ToLower().StartsWith("y"));
        }


        /// <summary>Gets the value for a field as a byte.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public byte GetByte(int i)
        {
            return byte.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the value for a field as a byte array.</summary>
        /// <param name="i">Ordinal.</param>
        /// <param name="fieldOffset">Field offset.</param>
        /// <param name="buffer">Buffer array.</param>
        /// <param name="bufferoffset">Buffer offset.</param>
        /// <param name="length">Length.</param>
        /// <returns>Number of bytes copied..</returns>
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            byte[] v = Encoding.UTF8.GetBytes(_Data[_Pos][i]);
            int j = 0;

            for(j = 0; j < length; j++)
            {
                if((fieldOffset + j) >= v.Length) break;
                buffer[bufferoffset + j] = v[fieldOffset + j];
            }

            return j;
        }


        /// <summary>Gets the value for a field as a character.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public char GetChar(int i)
        {
            return _Data[_Pos][i][0];
        }


        /// <summary>Gets the value for a field as a character array.</summary>
        /// <param name="i">Ordinal.</param>
        /// <param name="fieldOffset">Field offset.</param>
        /// <param name="buffer">Buffer array.</param>
        /// <param name="bufferoffset">Buffer offset.</param>
        /// <param name="length">Length.</param>
        /// <returns>Number of characters copied..</returns>
        public long GetChars(int i, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {
            int j = 0;
            int off = Convert.ToInt32(fieldOffset);

            for(j = 0; j < length; j++)
            {
                if((off + j) >= _Data[_Pos][i].Length) break;
                buffer[bufferoffset + j] = _Data[_Pos][i][off + j];
            }

            return j;
        }


        /// <summary>Gets a data reader for the field.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Data reader.</returns>
        /// <remarks>Not supported.</remarks>
        public IDataReader GetData(int i)
        {
            throw new NotSupportedException();
        }


        /// <summary>Gets the data type name for the field.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Data type name.</returns>
        /// <remarks>Not supported.</remarks>
        public string GetDataTypeName(int i)
        {
            throw new NotSupportedException();
        }


        /// <summary>Gets the field value as a DateTime value.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>DateTime value.</returns>
        public DateTime GetDateTime(int i)
        {
            // TODO: DateTime.
            throw new NotImplementedException();
        }


        /// <summary>Gets the field value as a decimal.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public decimal GetDecimal(int i)
        {
            return decimal.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field value as a double.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public double GetDouble(int i)
        {
            return double.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field type for this field.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Field type.</returns>
        public Type GetFieldType(int i)
        {
            return typeof(string);
        }


        /// <summary>Gets the field value as a float.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public float GetFloat(int i)
        {
            return float.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field value as a GUID.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public Guid GetGuid(int i)
        {
            return Guid.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field value as a 16-bit integer.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public short GetInt16(int i)
        {
            return short.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field value as a 32-bit integer.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public int GetInt32(int i)
        {
            return int.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field value as a 32-bit integer.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public long GetInt64(int i)
        {
            return long.Parse(_Data[_Pos][i]);
        }


        /// <summary>Gets the field name for this field.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Field name.</returns>
        public string GetName(int i)
        {
            return _ColumnNames[i];
        }


        /// <summary>Gets the ordinal for a field name.</summary>
        /// <param name="name">Field name.</param>
        /// <returns>Ordinal.</returns>
        public int GetOrdinal(string name)
        {
            for(int i = 0; i < _ColumnNames.Count; i++)
            {
                if(_ColumnNames[i].ToLower() == name.ToLower()) { return i; }
            }

            return -1;
        }


        /// <summary>Gets a schema table for this instance.</summary>
        /// <returns>Data table.</returns>
        public DataTable GetSchemaTable()
        {
            // TODO: schema table
            throw new NotImplementedException();
        }



        /// <summary>Gets the field value as a string.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public string GetString(int i)
        {
            return _Data[_Pos][i];
        }



        /// <summary>Gets the field value.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Value.</returns>
        public object GetValue(int i)
        {
            return _Data[_Pos][i];
        }


        /// <summary>Populates an array of objects with the column values of the current row.</summary>
        /// <param name="values">Array.</param>
        /// <returns>Numbers of objects copied</returns>
        public int GetValues(object[] values)
        {
            int i;
            for(i = 0; i < _Data[_Pos].Count; i++)
            {
                if(i >= values.Length) break;
                values[i] = _Data[_Pos][i];
            }

            return i;
        }


        /// <summary>Gets if the field value is NULL.</summary>
        /// <param name="i">Ordinal.</param>
        /// <returns>Returns TRUE if the value is NULL, otherwise returns FALSE.</returns>
        public bool IsDBNull(int i)
        {
            return _Data[_Pos][i] == "<<°---->&NULL>>";
        }


        /// <summary>Advances the data reader to the next result, when reading the results of batch SQL statements.</summary>
        /// <returns>Returns TRUE if there are more rows, otherwise returns FALSE.</returns>
        public bool NextResult()
        {
            return false;
        }


        /// <summary>Advances to the next record.</summary>
        /// <returns>Returns TRUE if there are more rows, otherwise returns FALSE.</returns>
        public bool Read()
        {
            return (++_Pos < _Data.Count);
        }
    }
}
