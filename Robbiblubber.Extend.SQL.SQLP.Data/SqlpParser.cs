﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements a parser for SQLP.</summary>
    public sealed class SqlpParser: SQLParser, IParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>SQLP provider.</summary>
        private SqlpProvider _Provider = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this clas.</summary>
        public SqlpParser(): base()
        {}


        /// <summary>Creates a new instance of this clas.</summary>
        /// <param name="provider">SQLP provider.</param>
        internal SqlpParser(SqlpProvider provider): base()
        {
            _Provider = provider;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLParser                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a source string.</summary>
        /// <param name="source">Source string.</param>
        /// <returns>Parsed SQL statements.</returns>
        public override IStatement[] Parse(string source)
        {
            switch(_Provider.SQLPProvider)
            {
                case "sqlite":
                    List<string> strings = _Parse(source);

                    IStatement[] rval = new IStatement[strings.Count];

                    for(int i = 0; i < strings.Count; i++)
                    {
                        rval[i] = new SqlpLiteStatement(strings[i]);
                    }

                    return rval;
            }

            return base.Parse(source);
        }
    }
}
