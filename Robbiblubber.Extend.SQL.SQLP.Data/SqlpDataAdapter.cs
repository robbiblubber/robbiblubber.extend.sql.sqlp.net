﻿using System;
using System.Data;
using System.Data.Common;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class provides a data adapter.</summary>
    public sealed class SqlpDataAdapter: DbDataAdapter, IDbDataAdapter, IDataAdapter
    {}
}
