﻿using System;
using System.Data;

using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.Library.Configuration;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements a data parameter.</summary>
    public sealed class SqlpParameter: IDbDataParameter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Database type.</summary>
        private DbType _DbType = DbType.String;

        /// <summary>Determines if the database type has been set.</summary>
        private bool _DbTypeSet = false;

        /// <summary>Parameter value.</summary>
        private object _Value = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpParameter()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        public SqlpParameter(string parameterName, object value)
        {
            DbType = DbType.String;
            Value = value;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="value">Parameter value.</param>
        /// <param name="type">Database type.</param>
        public SqlpParameter(string parameterName, object value, DbType type): this(parameterName, value)
        {
            DbType = type;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the data representation of this instance.</summary>
        internal string __Data
        {
            get
            {
                DdpString rval = new DdpString();

                rval.SetValue("name", ParameterName);
                
                if(Value == null)
                {
                    rval.SetValue("value", "");
                    rval.SetValue("type", "null");
                }
                else switch(_DbType)
                {
                    case DbType.AnsiString:
                    case DbType.AnsiStringFixedLength:
                    case DbType.String:
                    case DbType.StringFixedLength:
                        rval.SetValue("value", (string) _Value);
                        rval.SetValue("type", "string");
                        break;
                    case DbType.Byte:
                        rval.SetValue("value", Convert.ToInt32(Convert.ToByte(_Value)));
                        rval.SetValue("type", "byte");
                        break;
                    case DbType.Boolean:
                        rval.SetValue("value", (bool) _Value);
                        rval.SetValue("type", "bool");
                        break;
                    case DbType.Int16:
                        rval.SetValue("value", Convert.ToInt16(_Value).ToString());
                        rval.SetValue("type", "int16");
                        break;
                    case DbType.UInt16:
                        rval.SetValue("value", Convert.ToUInt16(_Value).ToString());
                        rval.SetValue("type", "uint16");
                        break;
                    case DbType.Int32:
                        rval.SetValue("value", Convert.ToInt32(_Value).ToString());
                        rval.SetValue("type", "int32");
                        break;
                    case DbType.UInt32:
                        rval.SetValue("value", Convert.ToUInt32(_Value).ToString());
                        rval.SetValue("type", "uint32");
                        break;
                    case DbType.Int64:
                        rval.SetValue("value", Convert.ToInt64(_Value).ToString());
                        rval.SetValue("type", "int64");
                        break;
                    case DbType.UInt64:
                        rval.SetValue("value", Convert.ToUInt64(_Value).ToString());
                        rval.SetValue("type", "uint64");
                        break;
                    case DbType.Single:
                        rval.SetValue("value", Convert.ToDouble(_Value).ToString().Replace(",", "."));
                        rval.SetValue("type", "single");
                        break;
                    case DbType.Double:
                        rval.SetValue("value", Convert.ToDouble(_Value).ToString().Replace(",", "."));
                        rval.SetValue("type", "double");
                        break;
                    case DbType.Decimal:
                        rval.SetValue("value", Convert.ToDecimal(_Value).ToString().Replace(",", "."));
                        rval.SetValue("type", "decimal");
                        break;
                    case DbType.Date:
                    case DbType.DateTime:
                    case DbType.DateTime2:
                    case DbType.Time:
                        rval.SetValue("value", (DateTime) _Value);
                        rval.SetValue("type", "datetime");
                        break;
                    default:
                        throw new NotSupportedException();
                }

                return rval.ToString(OutputFormat.BASE64);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataParameter                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database type.</summary>
        public DbType DbType
        {
            get { return _DbType; }
            set
            {
                _DbType = value;
                _DbTypeSet = true;
            }
        }


        /// <summary>Gets or sets the paramater direction.</summary>
        public ParameterDirection Direction
        {
            get { return ParameterDirection.Input; }
            set
            {
                if(value != ParameterDirection.Input)
                {
                    throw new NotSupportedException("Output parameters not supported.");
                }
            }
        }


        /// <summary>Gets if the parameter is nullable.</summary>
        public bool IsNullable
        {
            get { return true; }
        }


        /// <summary>Gets or sets the parameter name.</summary>
        public string ParameterName
        {
            get; set;
        }


        /// <summary>Gets or sets the source column.</summary>
        public string SourceColumn
        {
            get; set;
        }


        /// <summary>Gets or sets the source version.</summary>
        public DataRowVersion SourceVersion
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter value.</summary>
        public object Value
        {
            get { return _Value; }
            set
            {
                _Value = value;

                if(!_DbTypeSet)
                {
                    if(value is bool)
                    {
                        _DbType = DbType.Boolean;
                    }
                    else if(value is byte)
                    {
                        _DbType = DbType.Byte;
                    }
                    else if(value is short)
                    {
                        _DbType = DbType.Int16;
                    }
                    else if(value is int)
                    {
                        _DbType = DbType.Int32;
                    }
                    else if(value is long)
                    {
                        _DbType = DbType.Int64;
                    }
                    else if(value is ushort)
                    {
                        _DbType = DbType.UInt16;
                    }
                    else if(value is uint)
                    {
                        _DbType = DbType.UInt32;
                    }
                    else if(value is ulong)
                    {
                        _DbType = DbType.UInt64;
                    }
                    else if(value is string)
                    {
                        _DbType = DbType.String;
                    }
                    else if(value is float)
                    {
                        _DbType = DbType.Single;
                    }
                    else if(value is double)
                    {
                        _DbType = DbType.Double;
                    }
                    else if(value is decimal)
                    {
                        _DbType = DbType.Decimal;
                    }
                    else if(value is DateTime)
                    {
                        _DbType = DbType.DateTime;
                    }
                    else
                    {
                        _DbType = DbType.Binary;
                    }
                }
            }
        }


        /// <summary>Gets or sets the parameter precision.</summary>
        public byte Precision
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter scale.</summary>
        public byte Scale
        {
            get; set;
        }


        /// <summary>Gets or sets the parameter size.</summary>
        public int Size
        {
            get; set;
        }
    }
}
