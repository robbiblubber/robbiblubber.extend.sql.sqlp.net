﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements a parameter collection.</summary>
    public sealed class SqlpParameterCollection: IDataParameterCollection
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent command.</summary>
        private SqlpCommand _Command;
        
        /// <summary>Parameters.</summary>
        private List<SqlpParameter> _Parameters;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="cmd">Parent command.</param>
        internal SqlpParameterCollection(SqlpCommand cmd)
        {
            _Command = cmd;
            _Parameters = new List<SqlpParameter>();
            SyncRoot = new object();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the data representation of this instance.</summary>
        internal string __Data
        {
            get
            {
                string rval = "";
                bool first = true;

                foreach(SqlpParameter i in _Parameters)
                {
                    if(first)
                    {
                        first = false;
                    }
                    else { rval += "~"; }

                    rval += (i.__Data);
                }

                return rval;
            }
        }


        /// <summary>Gets the web parameters for this instance.</summary>
        internal string __WebParams
        {
            get
            {
                if(_Parameters.Count == 0)
                {
                    return "&hasp=false";
                }

                return "&hasp=true&p=" + __Data;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDataParameterCollection                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets a parameter.</summary>
        /// <param name="index">Index</param>
        public object this[int index]
        {
            get { return _Parameters[index]; }
            set
            {
                if(!(value is SqlpParameter)) { throw new ArgumentException("Expected SqlpParameter type argument."); }
                if(index == _Parameters.Count)
                {
                    Add(value);
                }
                else if(index > _Parameters.Count)
                {
                    throw new IndexOutOfRangeException();
                }

                List<SqlpParameter> rval = new List<SqlpParameter>();
                for(int i = 0; i < _Parameters.Count; i++)
                {
                    if(i == index)
                    {
                        rval.Add((SqlpParameter) value);
                    }
                    else { rval.Add(_Parameters[i]); }
                }

                _Parameters = rval;
            }
        }


        /// <summary>Gets or sets a parameter by its name.</summary>
        /// <param name="parameterName">Parameter name.</param>
        public object this[string parameterName]
        {
            get
            {
                foreach(SqlpParameter i in _Parameters)
                {
                    if(i.ParameterName == parameterName) { return i; }
                }

                throw new IndexOutOfRangeException();
            }
            set
            {
                int pos = IndexOf(parameterName);

                if(pos > -1)
                {
                    this[pos] = value;
                }
                else { Add(value); }
            }
        }


        /// <summary>Gets the number of elements in this collection.</summary>
        public int Count
        {
            get { return _Parameters.Count; }
        }


        /// <summary>Gets if the collection size is fixed.</summary>
        public bool IsFixedSize
        {
            get { return false; }
        }


        /// <summary>Gets if the collection is read-only.</summary>
        public bool IsReadOnly
        {
            get { return false; }
        }


        /// <summary>Gets if the collection is synchronized.</summary>
        public bool IsSynchronized
        {
            get { return false; }
        }


        /// <summary>Gets the synchronization object for this instance.</summary>
        public object SyncRoot
        {
            get; private set;
        }


        /// <summary>Adds a parameter to the collection.</summary>
        /// <param name="value">Parameter.</param>
        /// <returns>Index.</returns>
        public int Add(object value)
        {
            if(!(value is SqlpParameter)) { throw new ArgumentException("Expected SqlpParameter type argument."); }

            _Parameters.Add((SqlpParameter) value);
            return (_Parameters.Count - 1);
        }


        /// <summary>Clears the collection.</summary>
        public void Clear()
        {
            _Parameters.Clear();
        }


        /// <summary>Returns if the collection contains a parameter.</summary>
        /// <param name="value">Parameter.</param>
        /// <returns>Returns TRUE if the collection contains the parameter, otherwise returns FALSE.</returns>
        public bool Contains(object value)
        {
            return _Parameters.Contains(value);
        }


        /// <summary>Returns if the collection contains a parameter.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <returns>Returns TRUE if the collection contains the parameter, otherwise returns FALSE.</returns>
        public bool Contains(string parameterName)
        {
            foreach(SqlpParameter i in _Parameters)
            {
                if(i.ParameterName == parameterName) { return true; }
            }

            return false;
        }


        /// <summary>Copies the collection to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index at which copying begins.</param>
        public void CopyTo(Array array, int index)
        {
            for(int i = 0; i < _Parameters.Count; i++)
            {
                array.SetValue(_Parameters[i], index + i);
            }
        }


        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return _Parameters.GetEnumerator();
        }


        /// <summary>Gets the index of a parameter.</summary>
        /// <param name="value">Object.</param>
        /// <returns>Index, if the parameter is not found returns -1.</returns>
        public int IndexOf(object value)
        {
            if(!(value is SqlpParameter)) { throw new ArgumentException("Expected SqlpParameter type argument."); }

            return _Parameters.IndexOf((SqlpParameter) value);
        }


        /// <summary>Gets the index of a parameter.</summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <returns>Index, if the parameter is not found returns -1.</returns>
        public int IndexOf(string parameterName)
        {
            for(int i = 0; i < _Parameters.Count; i++)
            {
                if(_Parameters[i].ParameterName == parameterName) { return i; }
            }

            return -1;
        }


        /// <summary>Inserts a parameter into the collection.</summary>
        /// <param name="index">Index.</param>
        /// <param name="value">Parameter.</param>
        public void Insert(int index, object value)
        {
            if(!(value is SqlpParameter)) { throw new ArgumentException("Expected SqlpParameter type argument."); }

            if(index == _Parameters.Count)
            {
                Add(value);
            }
            else if(index > _Parameters.Count)
            {
                throw new IndexOutOfRangeException();
            }

            List<SqlpParameter> rval = new List<SqlpParameter>();
            for(int i = 0; i < _Parameters.Count; i++)
            {
                if(i == index)
                {
                    rval.Add((SqlpParameter) value);
                }
                rval.Add(_Parameters[i]);
            }

            _Parameters = rval;
        }


        /// <summary>Removes a parameter from the collection.</summary>
        /// <param name="value">Parameter.</param>
        public void Remove(object value)
        {
            if(!(value is SqlpParameter)) { throw new ArgumentException("Expected SqlpParameter type argument."); }
            _Parameters.Remove((SqlpParameter) value);
        }


        /// <summary>Removes a parameter from the collection.</summary>
        /// <param name="index">Index.</param>
        public void RemoveAt(int index)
        {
            _Parameters.RemoveAt(index);
        }


        /// <summary>Removes a parameter from the collection.</summary>
        /// <param name="parameterName">Parameter name.</param>
        public void RemoveAt(string parameterName)
        {
            int pos = IndexOf(parameterName);

            if(pos > -1) { _Parameters.RemoveAt(pos); }
        }
    }
}
