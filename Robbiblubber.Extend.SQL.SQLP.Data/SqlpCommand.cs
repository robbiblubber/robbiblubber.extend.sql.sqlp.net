﻿using System;
using System.Data;

using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.Library.Configuration;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements a SQLP command.</summary>
    public sealed class SqlpCommand: IDbCommand
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connection.</summary>
        private SqlpConnection _Connection;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="connection">Connection.</param>
        public SqlpCommand(SqlpConnection connection)
        {
            _Connection = connection;
            Parameters = new SqlpParameterCollection(this);
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbConnection                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the command text.</summary>
        public string CommandText
        {
            get; set;
        }


        /// <summary>Gets or sets the command timeout.</summary>
        public int CommandTimeout
        {
            get; set;
        }


        /// <summary>Gets or sets the command type.</summary>
        public CommandType CommandType
        {
            get; set;
        }


        /// <summary>Gets or sets the connection.</summary>
        public IDbConnection Connection
        {
            get { return _Connection; }
            set { _Connection = ((SqlpConnection) value); }
        }


        /// <summary>Gets the command paramters.</summary>
        public IDataParameterCollection Parameters
        {
            get; private set;
        }


        /// <summary>Gets or sets the current transactions.</summary>
        /// <remarks>Not supported by SQLP.</remarks>
        public IDbTransaction Transaction
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }


        /// <summary>Gets or sets the update row source.</summary>
        /// <remarks>Not supported by SQLP.</remarks>
        public UpdateRowSource UpdatedRowSource
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }


        /// <summary>Cancels the command.</summary>
        public void Cancel()
        {}


        /// <summary>Creates a parameter.</summary>
        /// <returns>Parameter.</returns>
        public IDbDataParameter CreateParameter()
        {
            return new SqlpParameter();
        }


        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {}


        /// <summary>Executes a non-query statement.</summary>
        /// <returns>Returns the number of rows modified.</returns>
        public int ExecuteNonQuery()
        {
            DdpString result = new DdpString(_Connection.__WC.DownloadString(_Connection.__URL + "/execute.php?handle=" + _Connection.__Handle + "&sql=" + CommandText.ToBase64() + ((SqlpParameterCollection) Parameters).__WebParams));

            if(!result.GetBoolean("success"))
            {
                throw new DataException(result.GetString("message"));
            }

            return result.GetInteger("result");
        }


        /// <summary>Executes a query and returns the results as a data reader.</summary>
        /// <returns>Data reader.</returns>
        IDataReader IDbCommand.ExecuteReader()
        {
            return ExecuteReader(CommandBehavior.Default);
        }


        /// <summary>Executes a query and returns the results as a data reader.</summary>
        /// <returns>Data reader.</returns>
        public SqlpReader ExecuteReader()
        {
            return ExecuteReader(CommandBehavior.Default);
        }


        /// <summary>Executes a query and returns the results as a data reader.</summary>
        /// <param name="behavior">Command behavior.</param>
        /// <returns>Data reader.</returns>
        IDataReader IDbCommand.ExecuteReader(CommandBehavior behavior)
        {
            return ExecuteReader(CommandBehavior.Default);
        }


        /// <summary>Executes a query and returns the results as a data reader.</summary>
        /// <param name="behavior">Command behavior.</param>
        /// <returns>Data reader.</returns>
        public SqlpReader ExecuteReader(CommandBehavior behavior)
        {
            string data = _Connection.__WC.DownloadString(_Connection.__URL + "/select.php?handle=" + _Connection.__Handle + "&sql=" + CommandText.ToBase64() + ((SqlpParameterCollection) Parameters).__WebParams);

            DdpString result = null;

            if(data.StartsWith("success=false;"))
            {
                result = result = new DdpString(data);
                throw new DataException(result.GetString("message"));
            }

            int p = data.IndexOf("data=");

            result = new DdpString(data.Substring(0, p));
            return new SqlpReader(result.GetString("meta"), data.Substring(p + 5, data.Length - p - 6));
        }


        /// <summary>Executes a query and returns a single result.</summary>
        /// <returns>Result.</returns>
        public object ExecuteScalar()
        {
            string s = _Connection.__WC.DownloadString(_Connection.__URL + "/single.php?handle=" + _Connection.__Handle + "&sql=" + CommandText.ToBase64() + ((SqlpParameterCollection) Parameters).__WebParams);
            DdpString result = new DdpString(_Connection.__WC.DownloadString(_Connection.__URL + "/single.php?handle=" + _Connection.__Handle + "&sql=" + CommandText.ToBase64() + ((SqlpParameterCollection) Parameters).__WebParams));

            if(!result.GetBoolean("success"))
            {
                throw new DataException(result.GetString("message"));
            }

            return result.GetString("result").FromBase64();
        }


        /// <summary>Prepares the statement.</summary>
        public void Prepare()
        {}
    }
}
