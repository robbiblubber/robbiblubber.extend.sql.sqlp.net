﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class provides a SQLite-specific implementation of the SQLStatement class for SQLP.</summary>
    public class SqlpLiteStatement: SQLStatement, IStatement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cosntructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpLiteStatement(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source.</param>
        public SqlpLiteStatement(string source): base(source)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLStatement                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the statement is a SELECT query.</summary>
        public override bool IsSelect
        {
            get { return (_LowerSource.StartsWith("select") || (_LowerSource.StartsWith("pragma") && (!_LowerSource.Contains("=")))); }
        }
    }
}
