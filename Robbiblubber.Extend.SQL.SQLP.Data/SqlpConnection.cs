﻿using System;
using System.Data;
using System.Net;

using Robbiblubber.Util.Library.Configuration;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements an SQLP database connection.</summary>
    public sealed class SqlpConnection: IDbConnection, IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Web Client.</summary>
        private WebClient _WC = null;
        
        /// <summary>Determines if web client has been generated automatically.</summary>
        private bool _AutoWC = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>API URL.</summary>
        internal string __URL;

        /// <summary>Connection handle.</summary>
        internal string __Handle;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpConnection()
        {
            Database = null;
            State = ConnectionState.Closed;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="connectionString">Connection string.</param>
        public SqlpConnection(string connectionString): this()
        {
            ConnectionString = connectionString;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="wc">Web client.</param>
        /// <param name="connectionString">Connection string.</param>
        public SqlpConnection(WebClient wc, string connectionString) : this(connectionString)
        {
            WC = wc;
            ConnectionString = connectionString;
        }




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the web client for this connection.</summary>
        public WebClient WC
        {
            get { return _WC; }
            set
            {
                _CleanupWC();
                _WC = value;
                _AutoWC = false;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the web client for this connection.</summary>
        internal WebClient __WC
        {
            get
            {
                if(_WC == null)
                {
                    _WC = new WebClient();
                    _WC.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    _AutoWC = true;
                }

                return _WC;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cleans up web client.</summary>
        private void _CleanupWC()
        {
            if((_WC != null) && _AutoWC)
            {
                _WC.Dispose();
                _WC = null;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbConnection                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the connection string for this connection.</summary>
        public string ConnectionString
        {
            get; set;
        }


        /// <summary>Gets the connection timeout for this connection.</summary>
        public int ConnectionTimeout
        {
            get { return 0; }
        }


        /// <summary>Gets the database name for this connection.</summary>
        public string Database
        {
            get; private set;
        }


        /// <summary>Gets the connection state for this connection.</summary>
        public ConnectionState State
        {
            get; internal set;
        }


        /// <summary>Begins a new transaction.</summary>
        /// <returns>Transaction.</returns>
        /// <remarks>Transactions are not supported.</remarks>
        public IDbTransaction BeginTransaction()
        {
            throw new NotSupportedException();
        }


        /// <summary>Begins a new transaction.</summary>
        /// <param name="il">Isolation level.</param>
        /// <returns>Transaction.</returns>
        /// <remarks>Transactions are not supported.</remarks>
        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            throw new NotSupportedException();
        }


        /// <summary>Changes the current database.</summary>
        /// <param name="databaseName">Database name.</param>
        public void ChangeDatabase(string databaseName)
        {
            throw new InvalidOperationException();
        }


        /// <summary>Closes the database.</summary>
        public void Close()
        {
            try
            {
                State = ConnectionState.Closed;
                
                DdpString result = new DdpString(__WC.DownloadString(__URL + "/close.php?handle=" + __Handle));

                __Handle = null;
                _CleanupWC();
            }
            catch(Exception)
            {
                throw new DataException("Failed to close connection.");
            }
        }


        /// <summary>Creates a command object.</summary>
        /// <returns>Command.</returns>
        public SqlpCommand CreateCommand()
        {
            return new SqlpCommand(this);
        }


        /// <summary>Creates a command object.</summary>
        /// <returns>Command.</returns>
        IDbCommand IDbConnection.CreateCommand()
        {
            return new SqlpCommand(this);
        }


        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {
            if(__WC != null) Close();
        }


        /// <summary>Opens the connection.</summary>
        public void Open()
        {
            DdpString ddp = new DdpString(ConnectionString);

            __URL = ddp.GetString("url").TrimEnd('/', ' ');

            bool first = true;
            string cstr = "/open.php?";
            foreach(string i in ddp.Keys)
            {
                if(i == "url") continue;

                if(first)
                {
                    first = false;
                }
                else { cstr += '&'; }

                cstr += (i + "=" + ddp.GetString(i));
            }

            try
            {
                DdpString result = new DdpString(__WC.DownloadString(__URL + cstr));

                __Handle = result.GetString("handle");

                if(!result.GetBoolean("success"))
                {
                    throw new Exception();
                }
            }
            catch(Exception)
            {
                throw new DataException("Connection failed.");
            }
        }
    }
}
