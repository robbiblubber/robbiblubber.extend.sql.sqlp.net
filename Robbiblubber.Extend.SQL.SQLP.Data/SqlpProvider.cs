﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Extend.SQL.SQLP.Data;
using Robbiblubber.Util.Library.Configuration;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Extend.SQL.SQLP.Data
{
    /// <summary>This class implements the SQLP database provider.</summary>
    public class SqlpProvider: SQLProvider, IProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpProvider(): base()
        {
            _Parser = new SqlpParser(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public SqlpProvider(string data): base(data)
        {
            _Parser = new SqlpParser(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the SQLP provider name.</summary>
        public string SQLPProvider
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DbProvider                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the provider configuration data.</summary>
        public override string Data
        {
            get
            {
                DdpString ddp = new DdpString();

                ddp["provider"] = ProviderName;
                ddp["enhanced"] = EnhancedString;

                SQLPProvider = EnhancedString.GetDdpValue("provider");

                if(SQLPProvider == "sqlite")
                {
                    string file = EnhancedString.GetDdpValue("file").Replace('\\', '/');
                    if(file.Contains("/")) { file = file.Substring(file.LastIndexOf('/') + 1); }
                    ddp["file"] = file;
                }

                return ddp.ToString();
            }
            set
            {
                DdpString ddp = new DdpString(value);

                EnhancedString = ddp["enhanced"];
            }
        }


        /// <summary>Connects to the database.</summary>
        public override void Connect()
        {
            _Connection = new SqlpConnection(EnhancedString);
            _Connection.Open();
        }


        /// <summary>Creates a data adapter.</summary>
        /// <param name="sql">SQL statement.</param>
        /// <returns>Data adapter.</returns>
        public override IDbDataAdapter CreateAdapter(string sql)
        {
            IDbDataAdapter rval = new SqlpDataAdapter();
            rval.SelectCommand = CreateCommand(sql);

            return rval;
        }


        /// <summary>Gets specific provider data.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Data.</returns>
        public override object GetData(string key)
        {
            if(key == "sqlpprovider")
            {
                return SQLPProvider;
            }
            return base.GetData(key);
        }
    }
}
