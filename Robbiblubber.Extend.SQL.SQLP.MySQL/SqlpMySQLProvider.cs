﻿using System;

using Robbiblubber.Extend.SQL.SQLP.Data;



namespace Robbiblubber.Extend.SQL.SQLP.MySQL
{
    /// <summary>This class implements the SQLP database provider for SQLite.</summary>
    public sealed class SqlpMySQLProvider: SqlpProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpMySQLProvider(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public SqlpMySQLProvider(string data): base(data)
        {}
    }
}
