﻿using System;
using System.Drawing;

using Robbiblubber.Util.SQL.Interpreter;



namespace Robbiblubber.Extend.SQL.SQLP.MySQL
{
    /// <summary>This class implements the SQLite database manager.</summary>
    public sealed class SqlpMySQLManager: SQLProviderManager, IProviderManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control.</summary>
        private IProviderControl _Control = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] SQLProviderManager                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the provider name.</summary>
        public override string ProviderName
        {
            get { return "Robbiblubber.Extend.SQL.SQLP.MySQL.SqlpMySQLProvider"; }
        }


        /// <summary>Gets the manager display name.</summary>
        public override string DisplayName
        {
            get { return "SQLP (MySQL)"; }
        }


        /// <summary>Gets the provider control.</summary>

        public override IProviderControl Control
        {
            get
            {
                if(_Control == null) { _Control = new SqlpMySQLProviderControl(); }

                return _Control;
            }
        }


        /// <summary>Gets the provider icon.</summary>
        public override Image ProviderIcon
        {
            get { return Resources.sqlp; }
        }


        /// <summary>Gets the connction icon.</summary>
        public override Image ConnectionIcon
        {
            get { return Resources.sqlpdb; }
        }
    }
}
