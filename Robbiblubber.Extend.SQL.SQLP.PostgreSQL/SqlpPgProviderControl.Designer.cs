﻿namespace Robbiblubber.Extend.SQL.SQLP.PostgreSQL
{
    partial class SqlpPgProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._TextHost = new System.Windows.Forms.TextBox();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelHost = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LabelPassword = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LabelUID = new System.Windows.Forms.Label();
            this._TextUID = new System.Windows.Forms.TextBox();
            this._LabelURL = new System.Windows.Forms.Label();
            this._TextURL = new System.Windows.Forms.TextBox();
            this._LabelDb = new System.Windows.Forms.Label();
            this._TextDb = new System.Windows.Forms.TextBox();
            this._LabelPort = new System.Windows.Forms.Label();
            this._TextPort = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _TextHost
            // 
            this._TextHost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextHost.Location = new System.Drawing.Point(30, 131);
            this._TextHost.Name = "_TextHost";
            this._TextHost.Size = new System.Drawing.Size(296, 25);
            this._TextHost.TabIndex = 2;
            this._TextHost.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 6;
            this._ButtonTest.Tag = "sqlpsi::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelHost
            // 
            this._LabelHost.AutoSize = true;
            this._LabelHost.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelHost.Location = new System.Drawing.Point(27, 115);
            this._LabelHost.Name = "_LabelHost";
            this._LabelHost.Size = new System.Drawing.Size(34, 13);
            this._LabelHost.TabIndex = 2;
            this._LabelHost.Tag = "sqlpsi::udiag.connect.host";
            this._LabelHost.Text = "&Host:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlpsi::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.Location = new System.Drawing.Point(233, 211);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 6;
            this._LabelPassword.Tag = "sqlpsi::udiag.connect.pwd";
            this._LabelPassword.Text = "Pass&word:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(236, 227);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(197, 25);
            this._TextPassword.TabIndex = 6;
            this._TextPassword.UseSystemPasswordChar = true;
            this._TextPassword.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelUID
            // 
            this._LabelUID.AutoSize = true;
            this._LabelUID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUID.Location = new System.Drawing.Point(27, 211);
            this._LabelUID.Name = "_LabelUID";
            this._LabelUID.Size = new System.Drawing.Size(65, 13);
            this._LabelUID.TabIndex = 5;
            this._LabelUID.Tag = "sqlpsi::udiag.connect.uid";
            this._LabelUID.Text = "&User Name:";
            // 
            // _TextUID
            // 
            this._TextUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUID.Location = new System.Drawing.Point(30, 227);
            this._TextUID.Name = "_TextUID";
            this._TextUID.Size = new System.Drawing.Size(197, 25);
            this._TextUID.TabIndex = 5;
            this._TextUID.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelURL
            // 
            this._LabelURL.AutoSize = true;
            this._LabelURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelURL.Location = new System.Drawing.Point(27, 67);
            this._LabelURL.Name = "_LabelURL";
            this._LabelURL.Size = new System.Drawing.Size(30, 13);
            this._LabelURL.TabIndex = 1;
            this._LabelURL.Tag = "sqlpsi::udiag.connect.url";
            this._LabelURL.Text = "&URL:";
            // 
            // _TextURL
            // 
            this._TextURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextURL.Location = new System.Drawing.Point(30, 83);
            this._TextURL.Name = "_TextURL";
            this._TextURL.Size = new System.Drawing.Size(403, 25);
            this._TextURL.TabIndex = 1;
            this._TextURL.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelDb
            // 
            this._LabelDb.AutoSize = true;
            this._LabelDb.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDb.Location = new System.Drawing.Point(27, 163);
            this._LabelDb.Name = "_LabelDb";
            this._LabelDb.Size = new System.Drawing.Size(58, 13);
            this._LabelDb.TabIndex = 4;
            this._LabelDb.Tag = "sqlpsi::udiag.connect.db";
            this._LabelDb.Text = "&Database:";
            // 
            // _TextDb
            // 
            this._TextDb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDb.Location = new System.Drawing.Point(30, 179);
            this._TextDb.Name = "_TextDb";
            this._TextDb.Size = new System.Drawing.Size(403, 25);
            this._TextDb.TabIndex = 4;
            this._TextDb.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelPort
            // 
            this._LabelPort.AutoSize = true;
            this._LabelPort.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPort.Location = new System.Drawing.Point(332, 115);
            this._LabelPort.Name = "_LabelPort";
            this._LabelPort.Size = new System.Drawing.Size(31, 13);
            this._LabelPort.TabIndex = 3;
            this._LabelPort.Tag = "sqlpsi::udiag.connect.port";
            this._LabelPort.Text = "&Port:";
            // 
            // _TextPort
            // 
            this._TextPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPort.Location = new System.Drawing.Point(335, 131);
            this._TextPort.Name = "_TextPort";
            this._TextPort.PasswordChar = '*';
            this._TextPort.Size = new System.Drawing.Size(98, 25);
            this._TextPort.TabIndex = 3;
            this._TextPort.UseSystemPasswordChar = true;
            this._TextPort.TextChanged += new System.EventHandler(this._Changed);
            // 
            // SqlpPgProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelPort);
            this.Controls.Add(this._TextPort);
            this.Controls.Add(this._LabelDb);
            this.Controls.Add(this._TextDb);
            this.Controls.Add(this._LabelPassword);
            this.Controls.Add(this._LabelURL);
            this.Controls.Add(this._TextPassword);
            this.Controls.Add(this._TextURL);
            this.Controls.Add(this._LabelUID);
            this.Controls.Add(this._TextUID);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelHost);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextHost);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SqlpPgProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextHost;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelHost;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.Label _LabelUID;
        private System.Windows.Forms.TextBox _TextUID;
        private System.Windows.Forms.Label _LabelURL;
        private System.Windows.Forms.TextBox _TextURL;
        private System.Windows.Forms.Label _LabelDb;
        private System.Windows.Forms.TextBox _TextDb;
        private System.Windows.Forms.Label _LabelPort;
        private System.Windows.Forms.TextBox _TextPort;
    }
}
