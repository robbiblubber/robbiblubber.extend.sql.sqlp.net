﻿using System;

using Robbiblubber.Extend.SQL.SQLP.Data;



namespace Robbiblubber.Extend.SQL.SQLP.PostgreSQL
{
    /// <summary>This class implements the SQLP database provider for SQLite.</summary>
    public sealed class SqlpPgProvider: SqlpProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpPgProvider(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public SqlpPgProvider(string data): base(data)
        {}
    }
}
