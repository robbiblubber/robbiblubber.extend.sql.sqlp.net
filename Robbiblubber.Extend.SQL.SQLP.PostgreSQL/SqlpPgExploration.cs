﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.SQL.Interpreter;



namespace Robbiblubber.Extend.SQL.SQLP.PostgreSQL
{
    /// <summary>This class implements an exploration provider.</summary>
    public class SqlpPgExploration: IExploration
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Explorer dictionary.</summary>
        protected Dictionary<ProviderItem, IExporer> _Explorers = new Dictionary<ProviderItem, IExporer>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpPgExploration(): base()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbExploration                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the explorer for a provider.</summary>
        /// <param name="provider">Provider.</param>
        /// <returns>Explorer.</returns>
        public IExporer this[ProviderItem provider]
        {
            get
            {
                if(!_Explorers.ContainsKey(provider))
                {
                    if(provider.ProviderName == "Robbiblubber.Extend.SQL.SQLP.PostgreSQL.SqlpPgProvider")
                    {
                        _Explorers.Add(provider, new SqlpPgExplorer(provider));
                    }
                    else { return null; }
                }

                try
                {
                    return _Explorers[provider];
                }
                catch(Exception) {}

                return null;
            }
        }
    }
}
