﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Library.Configuration;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Util.SQL.Interpreter;



namespace Robbiblubber.Extend.SQL.SQLP.PostgreSQL
{
    /// <summary>This class implements the SQLite provider control.</summary>
    public partial class SqlpPgProviderControl: UserControl, IProviderControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent provider.</summary>
        private ProviderItem _Provider;


        /// <summary>Tree node.</summary>
        private TreeNode _Node = null;


        /// <summary>Updating flag.</summary>
        private bool _Updating = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpPgProviderControl()
        {
            InitializeComponent();
            Node = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the enhanced string.</summary>
        private string _EnhancedString
        {
            get
            {
                DdpString rval = new DdpString();

                rval.SetValue("provider", "pgsql");
                rval.SetValue("url", _TextURL.Text);
                rval.SetValue("host", _TextHost.Text);
                rval.SetValue("port", _TextPort.Text);
                rval.SetValue("db", _TextDb.Text);
                rval.SetValue("uid", _TextUID.Text);
                rval.SetValue("pwd", _TextPassword.Text);

                return rval.ToString();
            }

            set
            {
                _Updating = true;

                DdpString ddp = new DdpString(value);

                _TextURL.Text = ddp.GetString("url");
                _TextHost.Text = ddp.GetString("host");
                _TextPort.Text = ddp.GetString("port");
                _TextDb.Text = ddp.GetString("db");
                _TextUID.Text = ddp.GetString("uid");
                _TextPassword.Text = ddp.GetString("pwd");

                _Updating = false;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>File name changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            if(!_Updating) { _Provider.Data = _Provider.Data.SetDdpValue("enhanced", _EnhancedString); }
        }


        /// <summary>Provider name changed.</summary>
        private void _TextName_TextChanged(object sender, EventArgs e)
        {
            _Provider.Name = _TextName.Text;

            if(Node != null) { Node.Text = _Provider.Name; }
        }


        /// <summary>Button "Test" click.</summary>
        private void _ButtonTest_Click(object sender, EventArgs e)
        {
            try
            {
                _Provider.Test();

                MessageBox.Show("sqlpsi::udiag.test.success".Localize("The connection has been tested successfully."), "sqlpsi::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("sqlpsi::udiag.test.fail".Localize("The test has failed.") + ' ' + ex.Message, "sqlpsi::udiag.test.caption".Localize("Test"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDbProviderControl                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the database provider.</summary>
        public ProviderItem Provider
        {
            get { return _Provider; }
            set
            {
                _Provider = value;

                _EnhancedString = _Provider.Data.GetDdpValue("enhanced");
                _TextName.Text = Provider.Name;
            }
        }


        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set { _Node = value; }
        }
    }
}
