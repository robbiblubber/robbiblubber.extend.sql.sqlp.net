﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Extend.SQL.SQLP.Data;



namespace Robbiblubber.Extend.SQL.SQLP.SQLite
{
    /// <summary>This class implements the SQLP database provider for SQLite.</summary>
    public sealed class SqlpLiteProvider: SqlpProvider
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SqlpLiteProvider(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Provider configuration data.</param>
        public SqlpLiteProvider(string data): base(data)
        {}
    }
}
