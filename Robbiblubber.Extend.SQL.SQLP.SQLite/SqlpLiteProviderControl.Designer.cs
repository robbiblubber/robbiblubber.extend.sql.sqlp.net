﻿namespace Robbiblubber.Extend.SQL.SQLP.SQLite
{
    partial class SqlpLiteProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._TextFile = new System.Windows.Forms.TextBox();
            this._ButtonTest = new System.Windows.Forms.Button();
            this._LabelFile = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._CheckSecurity = new System.Windows.Forms.CheckBox();
            this._PanelSecurity = new System.Windows.Forms.Panel();
            this._LabelPassword = new System.Windows.Forms.Label();
            this._TextPassword = new System.Windows.Forms.TextBox();
            this._LabelUID = new System.Windows.Forms.Label();
            this._TextUID = new System.Windows.Forms.TextBox();
            this._LabelURL = new System.Windows.Forms.Label();
            this._TextURL = new System.Windows.Forms.TextBox();
            this._PanelSecurity.SuspendLayout();
            this.SuspendLayout();
            // 
            // _TextFile
            // 
            this._TextFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextFile.Location = new System.Drawing.Point(30, 131);
            this._TextFile.Name = "_TextFile";
            this._TextFile.Size = new System.Drawing.Size(403, 25);
            this._TextFile.TabIndex = 2;
            this._TextFile.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _ButtonTest
            // 
            this._ButtonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonTest.Location = new System.Drawing.Point(286, 284);
            this._ButtonTest.Name = "_ButtonTest";
            this._ButtonTest.Size = new System.Drawing.Size(147, 29);
            this._ButtonTest.TabIndex = 6;
            this._ButtonTest.Tag = "sqlpsi::common.button.test";
            this._ButtonTest.Text = "&Test";
            this._ButtonTest.UseVisualStyleBackColor = true;
            this._ButtonTest.Click += new System.EventHandler(this._ButtonTest_Click);
            // 
            // _LabelFile
            // 
            this._LabelFile.AutoSize = true;
            this._LabelFile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFile.Location = new System.Drawing.Point(27, 115);
            this._LabelFile.Name = "_LabelFile";
            this._LabelFile.Size = new System.Drawing.Size(60, 13);
            this._LabelFile.TabIndex = 2;
            this._LabelFile.Tag = "sqlpsi::udiag.connect.file";
            this._LabelFile.Text = "&File Name:";
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Location = new System.Drawing.Point(27, 19);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "sqlpsi::udiag.connect.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(30, 35);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(403, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._TextName_TextChanged);
            // 
            // _CheckSecurity
            // 
            this._CheckSecurity.AutoSize = true;
            this._CheckSecurity.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CheckSecurity.Location = new System.Drawing.Point(30, 170);
            this._CheckSecurity.Name = "_CheckSecurity";
            this._CheckSecurity.Size = new System.Drawing.Size(160, 17);
            this._CheckSecurity.TabIndex = 3;
            this._CheckSecurity.Tag = "sqlpsi::udiag.connect.sec";
            this._CheckSecurity.Text = " Enable Advanced &Security";
            this._CheckSecurity.UseVisualStyleBackColor = true;
            this._CheckSecurity.CheckedChanged += new System.EventHandler(this._CheckSecurity_CheckedChanged);
            // 
            // _PanelSecurity
            // 
            this._PanelSecurity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelSecurity.Controls.Add(this._LabelPassword);
            this._PanelSecurity.Controls.Add(this._TextPassword);
            this._PanelSecurity.Controls.Add(this._LabelUID);
            this._PanelSecurity.Controls.Add(this._TextUID);
            this._PanelSecurity.Location = new System.Drawing.Point(18, 189);
            this._PanelSecurity.Name = "_PanelSecurity";
            this._PanelSecurity.Size = new System.Drawing.Size(426, 66);
            this._PanelSecurity.TabIndex = 7;
            // 
            // _LabelPassword
            // 
            this._LabelPassword.AutoSize = true;
            this._LabelPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelPassword.Location = new System.Drawing.Point(214, 10);
            this._LabelPassword.Name = "_LabelPassword";
            this._LabelPassword.Size = new System.Drawing.Size(59, 13);
            this._LabelPassword.TabIndex = 5;
            this._LabelPassword.Tag = "sqlpsi::udiag.connect.pwd";
            this._LabelPassword.Text = "Pass&word:";
            // 
            // _TextPassword
            // 
            this._TextPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPassword.Location = new System.Drawing.Point(217, 26);
            this._TextPassword.Name = "_TextPassword";
            this._TextPassword.PasswordChar = '*';
            this._TextPassword.Size = new System.Drawing.Size(197, 25);
            this._TextPassword.TabIndex = 5;
            this._TextPassword.UseSystemPasswordChar = true;
            this._TextPassword.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelUID
            // 
            this._LabelUID.AutoSize = true;
            this._LabelUID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelUID.Location = new System.Drawing.Point(8, 10);
            this._LabelUID.Name = "_LabelUID";
            this._LabelUID.Size = new System.Drawing.Size(65, 13);
            this._LabelUID.TabIndex = 4;
            this._LabelUID.Tag = "sqlpsi::udiag.connect.uid";
            this._LabelUID.Text = "&User Name:";
            // 
            // _TextUID
            // 
            this._TextUID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextUID.Location = new System.Drawing.Point(11, 26);
            this._TextUID.Name = "_TextUID";
            this._TextUID.Size = new System.Drawing.Size(197, 25);
            this._TextUID.TabIndex = 4;
            this._TextUID.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelURL
            // 
            this._LabelURL.AutoSize = true;
            this._LabelURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelURL.Location = new System.Drawing.Point(27, 67);
            this._LabelURL.Name = "_LabelURL";
            this._LabelURL.Size = new System.Drawing.Size(30, 13);
            this._LabelURL.TabIndex = 1;
            this._LabelURL.Tag = "sqlpsi::udiag.connect.url";
            this._LabelURL.Text = "&URL:";
            // 
            // _TextURL
            // 
            this._TextURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextURL.Location = new System.Drawing.Point(30, 83);
            this._TextURL.Name = "_TextURL";
            this._TextURL.Size = new System.Drawing.Size(403, 25);
            this._TextURL.TabIndex = 1;
            this._TextURL.TextChanged += new System.EventHandler(this._Changed);
            // 
            // SqlpLiteProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelURL);
            this.Controls.Add(this._TextURL);
            this.Controls.Add(this._PanelSecurity);
            this.Controls.Add(this._CheckSecurity);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelFile);
            this.Controls.Add(this._ButtonTest);
            this.Controls.Add(this._TextFile);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SqlpLiteProviderControl";
            this.Size = new System.Drawing.Size(463, 338);
            this._PanelSecurity.ResumeLayout(false);
            this._PanelSecurity.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _TextFile;
        private System.Windows.Forms.Button _ButtonTest;
        private System.Windows.Forms.Label _LabelFile;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.CheckBox _CheckSecurity;
        private System.Windows.Forms.Panel _PanelSecurity;
        private System.Windows.Forms.Label _LabelPassword;
        private System.Windows.Forms.TextBox _TextPassword;
        private System.Windows.Forms.Label _LabelUID;
        private System.Windows.Forms.TextBox _TextUID;
        private System.Windows.Forms.Label _LabelURL;
        private System.Windows.Forms.TextBox _TextURL;
    }
}
