﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Extend.SQL.SQLP.Data;



namespace Robbiblubber.Extend.SQL.SQLP.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlpConnection cn = new SqlpConnection("url=http://robbiblubber.org/util/sqlp/;provider=sqlite;file=../test/test.sqlite");
            cn.Open();

            SqlpCommand cmd = cn.CreateCommand();
            cmd.CommandText = "INSERT INTO BUSSI VALUES ( 1, 'Bussi', 'Bussi' )";
            cmd.ExecuteNonQuery();

            cmd = cn.CreateCommand();
            cmd.CommandText = "SELECT A, B FROM BUSSI";
            Console.WriteLine(cmd.ExecuteScalar());
            Console.ReadLine();

            cn.Close();
        }
    }
}
